﻿using System.Collections;
using BzKovSoft.ObjectSlicer;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class Blade : MonoBehaviour
{
	public float speed = 10f;
	public float accelerationTime = 1f;
	public bool isRun
	{
		get => _isRun;
		set {
			_isRun = value;
			StopAllCoroutines();
			StartCoroutine(Acceleration());
		} 
	}
	private bool _isRun = false;
	private float _speed = 0f;
	private Material _material;
	private int _sliceId = 0;
	private AudioSource _audioSource;

	private void Start()
	{
		_material = GetComponent<MeshRenderer>().material;
		_material.SetFloat("_speed", 0f);

		_audioSource = GetComponent<AudioSource>();
		_audioSource.volume = 0f;
	}

	private void OnTriggerEnter(Collider other)
	{
		IBzSliceableAsync sliceable = other.GetComponentInParent<IBzSliceableAsync>();
		_sliceId++;
		if(sliceable != null)
		{
			Plane plane = new Plane(transform.right, transform.position);
			if(plane.GetSide(other.transform.position))
			{
				plane.Flip();
			}
			sliceable.Slice(plane, _sliceId, null);
		}
	}

	private IEnumerator Acceleration()
	{
		while(_speed != (_isRun ? speed : 0f))
		{
			_speed += speed / accelerationTime * Time.deltaTime * (_isRun ? 1 : -1);
			if(_isRun && _speed > speed)
			{
				_speed = speed;
			}
			if(!_isRun && _speed < 0f)
			{
				_speed = 0f;
			}
			_material.SetFloat("_speed", _speed);
			_audioSource.volume = _speed / speed;
			_audioSource.pitch = _speed / speed;
			yield return null;
		}
	}
}
