﻿using UnityEngine;

public class Saw : MonoBehaviour
{
	public float distance;
	public float followingSpeed = 0.1f;

	public Blade blade;
	public GameObject sawObject;

	private Camera _camera;

	private void Start()
	{
		_camera = Camera.main;
	}

	void Update()
	{
		if(Input.GetMouseButton(0))
		{
			Vector3 newPosition = _camera.ScreenToWorldPoint(Input.mousePosition + new Vector3(0f, 0f, distance));
			Vector3 newForward = (newPosition - transform.position).normalized;
			Vector3 difForward = newForward - transform.forward;
			Vector3 forward = transform.forward + difForward * followingSpeed * Time.deltaTime;
			transform.rotation = Quaternion.LookRotation(forward, difForward);
			blade.isRun = true;
		}
		else
		{
			blade.isRun = false;
		}
	}
}
