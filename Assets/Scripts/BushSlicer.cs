﻿using BzKovSoft.ObjectSlicer;
using UnityEngine;

public class BushSlicer : BzSliceableObjectBase
{
	public float sliceImpulse = 2f;
	protected override BzSliceTryData PrepareData(Plane plane)
	{
		Collider[] colliders = gameObject.GetComponentsInChildren<Collider>();
		return new BzSliceTryData()
		{
			componentManager = new StaticComponentManager(gameObject, plane, colliders),
			plane = plane,
		};
	}
	protected override void OnSliceFinished(BzSliceTryResult result) 
	{
		if(result.sliced)
		{
			Rigidbody rigidbody = result.outObjectPos.GetComponent<Rigidbody>();
			if(rigidbody)
			{
				rigidbody.isKinematic = false;
				rigidbody.AddForce(result.outObjectPos.transform.forward * sliceImpulse, ForceMode.Impulse);
			}
		}
	}
}
