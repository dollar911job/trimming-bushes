﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
	public Image uiTimeFiller;
	public Image uiProgressFiller;

	public float walkingSpeed = 1f;
	public float stepDistance = 1f;
	public float totalDistance = 10f;
	public float stayTime = 10f;

	private int _steps = 0;
	private float _startDistance;
	private float _timeOffset;

	private void Start()
	{
		_startDistance = transform.position.z;
		StartCoroutine(BeginWalkCoroutine());
		StartCoroutine(UiUpdateCoroutine());
	}

	private IEnumerator BeginWalkCoroutine()
	{
		if(transform.position.z < _startDistance + totalDistance)
		{
			_timeOffset = Time.time;
			yield return new WaitForSeconds(stayTime);
			_steps++;
			StartCoroutine(ProcessWalkCoroutine());
		}
		else
		{
			StopAllCoroutines();
		}
	}

	private IEnumerator ProcessWalkCoroutine()
	{
		while(transform.position.z < _startDistance + stepDistance * _steps)
		{
			transform.position += Vector3.forward * walkingSpeed * Time.deltaTime;
			yield return null;
		}
		StartCoroutine(BeginWalkCoroutine());
	}

	private IEnumerator UiUpdateCoroutine()
	{
		while(true)
		{
			uiProgressFiller.fillAmount = (transform.position.z - _startDistance) / totalDistance;
			uiTimeFiller.fillAmount = (Time.time - _timeOffset) / stayTime;
			yield return null;
		}
	}
}
